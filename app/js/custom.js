$(document).ready(function() {

    function fullPage() {

        var transitionSpeed = 1500;

        $('#fullpage').fullpage({
            autoScrolling: true,
            fitToSection: true,
            scrollingSpeed: transitionSpeed,
            
            onLeave: function(origin, destination, direction) {
       
                // Toggles a class for handling the second section's rotating messages
                if ( destination.index == 1 ) {
                    $('#sectionTwo').addClass('fpRotateMessaging');
                } else {
                    setTimeout( function() {
                        $('#sectionTwo').removeClass('fpRotateMessaging');
                    }, transitionSpeed )
                }
    
                // Toggles a class to break-out of FullPage.js restrictions if it's the last slide
                if ( destination.isLast ) {
                    setTimeout( function() {
                        $('html').addClass('fp-scroll-free');
                    }, transitionSpeed );
                }
    
                // Toggles classes & reactivates FullPage.js if scrolling up from the last slide
                // We check the scroll position so that it only fires if we're 1 scroll from the top
                var scroll = $(window).scrollTop();
                if ( scroll < 101 ) {
                    if ( origin.isLast && direction == 'up' ) {
                        setTimeout( function() {
                            $('html').removeClass('fp-scroll-free');
                        }, (transitionSpeed / 2) );
                    }
                } else if ( origin.isLast && direction == 'up' ) {
                    // Returning false tells FullPage.js to do nothing
                    return false;
                } else {
                    return false;
                }
            },
            
            afterResize: function(width) {
                if ( width < 1024 ) {
                    $.fn.fullpage.setAutoScrolling(false);
                    $.fn.fullpage.setFitToSection(false);
                    $('html').addClass('fp-scroll-free');
                }
                if ( width >= 1024 ) {
                    $.fn.fullpage.setAutoScrolling(true);
                    $.fn.fullpage.setFitToSection(true);
                    $('html').removeClass('fp-scroll-free');
                }
            },

            // afterRender: function() {
            //     console.log("FullPage Rendered");
            //     $('.tw-fp .section').toggleClass('fpRendered');
            // }

        });
    }
    if ( window.innerWidth >= 1024 ) {
        // Double check the windows size before firing the function for performance reasons
        fullPage();
    }

    function sectionTwoInView() {
        // Adds a class to apply animations to the second section on smaller devices
        // - Uses the existing inView library
        var target = $('#sectionTwo');
        if ( window.innerWidth < 1024 ) {
            inView('#sectionTwo').once('enter', function(el) {
                $(target).addClass('fpRotateMessaging');
                console.log("Section 2 is visible.");
            });
        }
    }
    sectionTwoInView();
	
});